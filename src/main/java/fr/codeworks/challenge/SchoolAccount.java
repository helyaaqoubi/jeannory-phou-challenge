package fr.codeworks.challenge;

public class SchoolAccount implements Account {
    @Override
    public void processLocalTransfer() {
        System.out.println("SchoolAccount::BusinessLogin");
    }

    @Override
    public void processInternationalTransfer() {
        throw new RuntimeException("Not Authorized");
    }
}
package fr.codeworks.challenge;

public interface Account {
    void processLocalTransfer();
    void processInternationalTransfer();
}
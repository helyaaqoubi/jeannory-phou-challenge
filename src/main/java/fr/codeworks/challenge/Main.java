package fr.codeworks.challenge;

public class Main {

    public static void main(String[] args) {
        final Account account = new SchoolAccount();

        account.processLocalTransfer();

        account.processInternationalTransfer();
    }
}